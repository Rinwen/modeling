#include "pch.hpp"

int main(int argc, char** argv)
{
	try
	{
		auto state = networks::createSimulationState(networks::getConfig(argc, argv));
		const auto modelType = state.modelType;
		const auto steps = state.steps;

		const auto sim = networks::simulation::cuda::CreateSimulation(std::move(state), modelType, networks::createEventLoop());

		for (auto currentStep = 0; currentStep < steps; ++currentStep)
		{
			auto stats = sim->update();
			for (auto it = stats.first; it != stats.second; ++it)
			{
				std::cout << *it << ' ';
			}
			std::cout << std::endl;
		}
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
		return -1;
	}

	return 0;
}