#include <cub/device/device_scan.cuh>

#include "ExclusiveScanList.cuh"

using namespace networks;
using namespace simulation;
using namespace cuda;

ExclusiveScanList::ExclusiveScanList(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream) :
	m_cubInternalStorage(cubInternalStorage), 
	m_stream(stream)
{
	if (cubInternalStorage == nullptr)
		throw std::invalid_argument("internal storage is null");

	if (stream == nullptr)
		throw std::invalid_argument("stream storage is null");
}

void ExclusiveScanList::scan(const TrivialDeviceArray<int8_t>& values)
{
	m_result.resize(values.size(), *m_stream);

	size_t storageBytes;
	CudaSafeCall(cub::DeviceScan::ExclusiveSum(nullptr, storageBytes, values.data(), m_result.data(), int(values.size()), m_stream->get()));
	m_cubInternalStorage->resize(storageBytes, *m_stream);
	CudaSafeCall(cub::DeviceScan::ExclusiveSum(m_cubInternalStorage->data(), storageBytes, values.data(), m_result.data(), int(values.size()), m_stream->get()));
}

void ExclusiveScanList::scan(const TrivialDeviceArray<uint32_t>& values)
{
	m_result.resize(values.size(), *m_stream);

	size_t storageBytes;
	CudaSafeCall(cub::DeviceScan::ExclusiveSum(nullptr, storageBytes, values.data(), m_result.data(), int(values.size()), m_stream->get()));
	m_cubInternalStorage->resize(storageBytes, *m_stream);
	CudaSafeCall(cub::DeviceScan::ExclusiveSum(m_cubInternalStorage->data(), storageBytes, values.data(), m_result.data(), int(values.size()), m_stream->get()));
}

const TrivialDeviceArray<int32_t>& ExclusiveScanList::result() const noexcept
{
	return m_result;
}