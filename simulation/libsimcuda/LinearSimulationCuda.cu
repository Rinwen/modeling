#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <cmath>
#include <numeric>
#include <utility>

#include "cuda/cuda-helpers.cuh"
#include "SimCommon.cuh"
#include "LinearSimulationCuda.cuh"

using namespace networks::simulation;
using namespace networks::simulation::cuda;

static float __device__ squareWave(unsigned long long step, int period)
{
	step = step % period;
	return step < (period / 2) ? 1.0f : -1.0f;
}

static float __device__ sineWave(unsigned long long step, int period)
{
	static constexpr auto doublePi = float(M_PI * 2.0);

	step = step % period;
	auto normalized = float(step) / float(period);
	return std::sin(normalized * doublePi);
}

static float __device__ waveSample(SimulationState::Waveform waveform, unsigned long long step, int period)
{
	switch (waveform)
	{
	case SimulationState::Waveform::Square:
		return squareWave(step, period);
		break;
	case SimulationState::Waveform::Sine:
		return sineWave(step, period);
		break;
	default:
		return 0.0f;
	}
}

LinearSimulationCuda::LinearSimulationCuda(SimulationState&& state_)
{
	const auto state = std::move(state_);

	m_clusters = makeClusters(state.clusterSizes);
	m_waveform = state.waveform;
	m_period = state.period;
	m_mu = state.mu;

	m_nodesState = makeInitialState(m_clusters, state.hotCluster);
	m_stateDerivative.resize(m_nodesState.size(), *m_stream);
	m_nextState.resize(m_nodesState.size(), *m_stream);

	m_edgeList = EdgeListStorage(state.adjacencyMatrix, m_clusters, state.clusterConnections, m_cubInternalStorage, m_stream);
	m_activeToInactives = EdgeListStorage(m_edgeList.size(), m_cubInternalStorage, m_stream);
	m_activeToSingleInactive = EdgeListStorage(m_edgeList.size(), m_cubInternalStorage, m_stream);
	m_activeToSingleInactiveSorted = EdgeListStorage(m_edgeList.size(), m_cubInternalStorage, m_stream);
	m_singleActiveToSingleInactive = EdgeListStorage(m_edgeList.size(), m_cubInternalStorage, m_stream);

	m_vote = RandomSelectorRLE(m_cubInternalStorage, m_stream);
	m_sorted = SortedKeyValueList(m_cubInternalStorage, m_stream);

	m_activityScan = InclusiveScanList(m_cubInternalStorage, m_stream);
	m_deviceActivityBatch.resize(m_nodesState.size() * kBatchSize, *m_stream);
	m_hostActivityScanBatch.resize(m_nodesState.size() * kBatchSize);
	CudaSafeCall(cudaEventRecord(m_activityScanCompleteEvent.get(), m_stream->get()));
	CudaSafeCall(cudaEventRecord(m_downloadEvent.get(), m_downloadStream.get()));
}

static __global__ void ExtinguishKernel(const unsigned nodesCount, const int32_t* __restrict__ state, const uint32_t* __restrict__ randomValues, const float mu, int8_t* __restrict__ derivative)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= nodesCount)
		return;

	auto result = int8_t(0);
	if (state[threadId] && float(randomValues[threadId]) / float(0xFFFFFFFF) <= mu)
		result = -1;

	derivative[threadId] = result;
}

static __global__ void IgniteKernel(
	const unsigned edgesCount,
	SimulationState::Waveform waveform,
	int period,
	const unsigned long long* __restrict__ stepPtr,
	const EdgeListSOA edges,
	const uint32_t* __restrict__ randomValues,
	int8_t* __restrict__ derivative)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= edgesCount)
		return;

	const auto to = edges.to[threadId];
	if (to == uint32_t(-1))
		return;

	const auto sample = waveSample(waveform, *stepPtr, period);

	const auto lambda = edges.lambda[threadId];
	const auto delta = edges.delta[threadId];
	const auto currentLambda = lambda + sample * delta;

	if (float(randomValues[threadId]) / float(0xFFFFFFFF) > currentLambda)
		return;

	derivative[to] = +1;
}

static __global__ void RollbackKernel(const unsigned nodesCount, const int32_t* __restrict__ scanResult, const int32_t* __restrict__ state, int32_t* __restrict__ nextState, unsigned long long* __restrict__ stepPtr)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= nodesCount)
		return;

	const auto totalActiveCount = scanResult[nodesCount - 1];
	if (totalActiveCount)
	{
		if (threadId == 0)
		{
			(*stepPtr)++;
		}
		return;
	}

	nextState[threadId] = state[threadId];
}

std::pair<const float*, const float*> LinearSimulationCuda::update()
{
	const auto statsSize = m_clusters.size() + 1;
	if (!m_statsBuffer.empty())
	{
		m_statsBuffer.erase(m_statsBuffer.begin(), m_statsBuffer.begin() + statsSize);
	}

	while (m_statsBuffer.empty())
	{
		if (m_randomStep == 0)
		{
			const auto stepSize = m_edgeList.size() * 2 + m_nodesState.size() * 2;
			m_generator.generate(m_randomValues, kRandomSteps * stepSize);
		}

		edgeSelection();
		computeDerivative();
		updateNextState();
		updateStats();
		downloadNextState();

		std::swap(m_nextState, m_nodesState);

		m_randomStep++;
		m_randomStep = m_randomStep % kRandomSteps;

		m_batchWriteIdx++;
		m_batchWriteIdx = m_batchWriteIdx % kBatchSize;
	}
	
	return std::make_pair(m_statsBuffer.data(), m_statsBuffer.data() + statsSize);
}

void LinearSimulationCuda::edgeSelection()
{
	const auto stepSize = m_edgeList.size() * 2 + m_nodesState.size() * 2;
	const auto d_vote1 = m_randomValues.data() + m_randomStep * stepSize;
	const auto d_vote2 = d_vote1 + m_edgeList.size();

	m_edgeList.filter(m_nodesState, m_activeToInactives);
	m_vote.vote(m_activeToInactives.from(), d_vote1);
	m_activeToInactives.gather(m_vote.indices(), m_activeToSingleInactive);
	m_sorted.sort(m_activeToSingleInactive.to());
	m_activeToSingleInactive.gather(m_sorted.indices(), m_activeToSingleInactiveSorted);
	m_vote.vote(m_activeToSingleInactiveSorted.to(), d_vote2);
	m_activeToSingleInactiveSorted.gather(m_vote.indices(), m_singleActiveToSingleInactive);
}

void LinearSimulationCuda::computeDerivative()
{
	const auto nodesCount = unsigned(m_nodesState.size());
	const auto stepSize = m_edgeList.size() * 2 + m_nodesState.size() * 2;
	const auto d_extinguishRandom = m_randomValues.data() + m_randomStep * stepSize + m_edgeList.size() * 2;
	const auto d_ignitionRandom = d_extinguishRandom + m_nodesState.size();

	ExtinguishKernel <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get() >>> (nodesCount, m_nodesState.data(), d_extinguishRandom, m_mu, m_stateDerivative.data());
	CudaCheckError();

	const auto edgesCount = unsigned(m_singleActiveToSingleInactive.size());
	IgniteKernel <<<gridSize(edgesCount), kBlockSize, 0, m_stream->get() >>> (
		edgesCount,
		m_waveform,
		m_period,
		m_step.data(),
		m_singleActiveToSingleInactive.get(),
		d_ignitionRandom,
		m_stateDerivative.data());
	CudaCheckError();
}

void LinearSimulationCuda::updateNextState()
{
	const auto nodesCount = unsigned(m_nodesState.size());
	const auto scanBytesSize = nodesCount * sizeof(int32_t);

	NextStateKernel <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get() >>> (nodesCount, m_nodesState.data(), m_stateDerivative.data(), m_nextState.data());
	CudaCheckError();

	if (m_batchWriteIdx == 0)
	{
		CudaSafeCall(cudaStreamWaitEvent(m_stream->get(), m_downloadEvent.get(), 0));
	}
	m_activityScan.scan(m_nextState);
	CudaSafeCall(cudaMemcpyAsync(m_deviceActivityBatch.data() + m_batchWriteIdx * nodesCount, m_activityScan.result().data(), scanBytesSize, cudaMemcpyDeviceToDevice, m_stream->get()));
	if (m_batchWriteIdx == kBatchSize - 1)
	{
		CudaSafeCall(cudaEventRecord(m_activityScanCompleteEvent.get(), m_stream->get()));
	}

	RollbackKernel <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get() >>> (nodesCount, m_activityScan.result().data(), m_nodesState.data(), m_nextState.data(), m_step.data());
	CudaCheckError();
}

void LinearSimulationCuda::updateStats()
{
	if (m_batchWriteIdx != 0)
		return;

	CudaSafeCall(cudaEventSynchronize(m_downloadEvent.get()));

	const auto nodesCount = m_nodesState.size();
	const auto h_scanBatch = m_hostActivityScanBatch.data();

	for (size_t scanIdx = 0; scanIdx < kBatchSize; ++scanIdx)
	{
		const auto h_scan = h_scanBatch + scanIdx * nodesCount;
		const auto activeNodes = h_scan[nodesCount - 1];

		// LinearSimulationCuda: skip empty silent
		if (activeNodes == 0)
			continue;

		for (size_t i = 0; i < m_clusters.size(); ++i)
		{
			const auto range = clusterRange(m_clusters, i);
			auto scan1 = uint32_t(0);
			auto scan2 = uint32_t(0);

			if (range.first != 0)
				scan1 = h_scan[range.first - 1];
			scan2 = h_scan[range.second - 1];

			m_statsBuffer.emplace_back(float(scan2 - scan1) / float(range.second - range.first));
		}
		m_statsBuffer.emplace_back(float(activeNodes) / float(m_clusters.back()));
	}
}

void LinearSimulationCuda::downloadNextState()
{
	if (m_batchWriteIdx != kBatchSize - 1)
		return;

	const auto nodesCount = m_nodesState.size();
	const auto scanBytesSize = nodesCount * sizeof(int32_t);
	const auto batchBytesSize = scanBytesSize * kBatchSize;

	CudaSafeCall(cudaStreamWaitEvent(m_downloadStream.get(), m_activityScanCompleteEvent.get(), 0));
	CudaSafeCall(cudaMemcpyAsync(m_hostActivityScanBatch.data(), m_deviceActivityBatch.data(), batchBytesSize, cudaMemcpyDeviceToHost, m_downloadStream.get()));
	CudaSafeCall(cudaEventRecord(m_downloadEvent.get(), m_downloadStream.get()));
}