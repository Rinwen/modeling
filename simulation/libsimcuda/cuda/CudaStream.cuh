#pragma once

#include <cuda_runtime.h>

namespace networks {
namespace simulation {
namespace cuda {

class CudaStream
{
public:
	CudaStream();
	CudaStream(const CudaStream&) = delete;
	CudaStream(CudaStream&& orig) noexcept;
	~CudaStream();

	CudaStream& operator=(const CudaStream&) = delete;
	CudaStream& operator=(CudaStream&& orig) noexcept;

	cudaStream_t get() const noexcept;
private:
	cudaStream_t m_handle = nullptr;
};

}
}
}