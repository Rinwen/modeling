#include <stdexcept>
#include <string>

#include "CurandGenerator.cuh"

using namespace networks::simulation::cuda;

CurandGenerator::CurandGenerator(unsigned long long seed, const std::shared_ptr<CudaStream>& stream) :
	m_stream(stream)
{
	auto status = curandCreateGenerator(&m_handle, CURAND_RNG_PSEUDO_MT19937);
	if (status == CURAND_STATUS_SUCCESS)
	{
		status = curandSetPseudoRandomGeneratorSeed(m_handle, seed);
	}

	if (status == CURAND_STATUS_SUCCESS)
	{
		status = curandSetStream(m_handle, m_stream->get());
	}

	if (status != CURAND_STATUS_SUCCESS)
	{
		if (m_handle)
		{
			curandDestroyGenerator(m_handle);
		}
		throw std::runtime_error("curand status " + std::to_string(status));
	}
}

CurandGenerator::CurandGenerator(long long seed, const std::shared_ptr<CudaStream>& stream) :
	CurandGenerator(unsigned long long(seed), stream)
{

}

CurandGenerator::CurandGenerator(CurandGenerator&& orig) noexcept
{
	*this = std::move(orig);
}
CurandGenerator::~CurandGenerator()
{
	if (m_handle)
		curandDestroyGenerator(m_handle);
}

CurandGenerator& CurandGenerator::operator=(CurandGenerator&& orig) noexcept
{
	std::swap(m_handle, orig.m_handle);
	return *this;
}

void CurandGenerator::generate(TrivialDeviceArray<uint32_t>& out, size_t size)
{
	out.resize(size, *m_stream);
	CurandSafeCall(curandGenerate(m_handle, out.data(), size));
}