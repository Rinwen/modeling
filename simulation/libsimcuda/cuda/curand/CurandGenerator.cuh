#pragma once

#include <memory>

#include <curand.h>

#include "../CudaStream.cuh"
#include "../CudaEvent.cuh"
#include "../TrivialDeviceArray.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class CurandGenerator
{
public:
	CurandGenerator() = delete;
	CurandGenerator(unsigned long long seed, const std::shared_ptr<CudaStream>& stream);
	CurandGenerator(long long seed, const std::shared_ptr<CudaStream>& stream);
	CurandGenerator(const CurandGenerator&) = delete;
	CurandGenerator(CurandGenerator&&) noexcept;
	~CurandGenerator();

	CurandGenerator& operator=(const CurandGenerator&) = delete;
	CurandGenerator& operator=(CurandGenerator&&) noexcept;

	void generate(TrivialDeviceArray<uint32_t>& out, size_t size);
private:
	curandGenerator_t m_handle = nullptr;
	std::shared_ptr<CudaStream> m_stream = nullptr;
};

}
}
}