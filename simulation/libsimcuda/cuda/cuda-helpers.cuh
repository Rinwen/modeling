#pragma once
#include <cuda.h>
#include <cuda_runtime.h>
#include <curand.h>

#include <iostream>
#include <sstream>
#include <stdexcept>

namespace networks {
namespace simulation {
namespace cuda {

constexpr unsigned kBlockSize = 64;

inline unsigned gridSize(unsigned elements, unsigned blockSize = kBlockSize)
{
	if (elements == 0)
		return 0;
	return (elements - 1) / blockSize + 1;
}

inline void __cudaSafeCall(cudaError err, const char* function, const char* file, const int line)
{
	if (err != cudaSuccess)
	{
		std::stringstream errorStream;
		errorStream << "CUDA error occured at " << function << ' ' << file << ':' << line << ' ' << cudaGetErrorString(err);
		std::string errorMsg = errorStream.str();
		throw std::runtime_error(std::move(errorMsg));
	}
}
#define CudaSafeCall(err) networks::simulation::cuda::__cudaSafeCall(err, __FUNCTION__, __FILE__, __LINE__ )


inline void __curandSafeCall(curandStatus_t err, const char* function, const char* file, const int line)
{
	if (err != CURAND_STATUS_SUCCESS)
	{
		std::stringstream errorStream;
		errorStream << "Curand error occured at " << function << ' ' << file << ':' << line << ' ' << std::to_string(err);
		std::string errorMsg = errorStream.str();
		throw std::runtime_error(std::move(errorMsg));
	}
}
#define CurandSafeCall(err) networks::simulation::cuda::__curandSafeCall(err, __FUNCTION__, __FILE__, __LINE__ )

inline void __cudaCheckError(const char* function, const char* file, const int line)
{
	//__cudaSafeCall(cudaDeviceSynchronize(), function, file, line);
	//__cudaSafeCall(cudaGetLastError(), function, file, line);
}

#define CudaCheckError()  networks::simulation::cuda::__cudaCheckError(__FUNCTION__, __FILE__, __LINE__ )

}
}
}
