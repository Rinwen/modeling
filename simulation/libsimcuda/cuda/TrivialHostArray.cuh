#pragma once

#include <cuda_runtime.h>
#include <stdexcept>
#include <vector>

#include "cuda-helpers.cuh"

namespace networks {
namespace simulation {
namespace cuda {

template<typename T>
class TrivialHostArray
{
public:
	TrivialHostArray() = default;
	TrivialHostArray(size_t size);
	TrivialHostArray(const std::vector<T>& hostData);
	TrivialHostArray(const TrivialHostArray&) = delete;
	TrivialHostArray(TrivialHostArray&& orig) noexcept;
	~TrivialHostArray();

	TrivialHostArray& operator=(const TrivialHostArray&) = delete;
	TrivialHostArray& operator=(TrivialHostArray&& orig) noexcept;

	T* data() const noexcept;
	size_t size() const noexcept;
	size_t capacity() const noexcept;

	void resize(size_t newSize);
private:
	T* m_pinnedHostMemory = nullptr;
	size_t m_size = 0;
	size_t m_capacity = 0;
};

template<typename T>
TrivialHostArray<T>::TrivialHostArray(size_t size) : m_size(size), m_capacity(size)
{
	const auto status = cudaHostAlloc(&m_pinnedHostMemory, size * sizeof(T), cudaHostAllocDefault);
	if (status == cudaErrorMemoryAllocation)
	{
		throw std::bad_alloc();
	}
	else
	{
		CudaSafeCall(status);
	}
}

template<typename T>
TrivialHostArray<T>::TrivialHostArray(const std::vector<T>& hostData) : TrivialHostArray(hostData.size())
{
	CudaSafeCall(cudaMemcpy(m_pinnedHostMemory, hostData.data(), hostData.size() * sizeof(T), cudaMemcpyHostToHost));
}

template<typename T>
TrivialHostArray<T>::TrivialHostArray(TrivialHostArray&& orig) noexcept
{
	*this = std::move(orig);
}

template<typename T>
TrivialHostArray<T>::~TrivialHostArray()
{
	if (m_pinnedHostMemory)
		cudaFreeHost(m_pinnedHostMemory);
}

template<typename T>
TrivialHostArray<T>& TrivialHostArray<T>::operator=(TrivialHostArray&& orig) noexcept
{
	std::swap(orig.m_pinnedHostMemory, m_pinnedHostMemory);
	std::swap(orig.m_size, m_size);
	std::swap(orig.m_capacity, m_capacity);
	return *this;
}

template<typename T>
T* TrivialHostArray<T>::data() const noexcept
{
	return m_pinnedHostMemory;
}

template<typename T>
size_t TrivialHostArray<T>::size() const noexcept
{
	return m_size;
}

template<typename T>
size_t TrivialHostArray<T>::capacity() const noexcept
{
	return m_capacity;
}

template<typename T>
void TrivialHostArray<T>::resize(size_t newSize)
{
	if (newSize <= m_capacity)
	{
		m_size = newSize;
	}
	else
	{
		*this = TrivialHostArray<T>(newSize);
	}
}

}
}
}