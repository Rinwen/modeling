#include "CudaStream.cuh"
#include "cuda-helpers.cuh"

using namespace networks::simulation::cuda;

CudaStream::CudaStream()
{
	CudaSafeCall(cudaStreamCreate(&m_handle));
}

CudaStream::CudaStream(CudaStream&& orig) noexcept
{
	*this = std::move(orig);
}

CudaStream::~CudaStream()
{
	if (m_handle)
		cudaStreamDestroy(m_handle);
}

CudaStream& CudaStream::operator=(CudaStream&& orig) noexcept
{
	std::swap(orig.m_handle, m_handle);
	return *this;
}

cudaStream_t CudaStream::get() const noexcept
{
	return m_handle;
}