#pragma once

#include <cuda_runtime.h>

namespace networks {
namespace simulation {
namespace cuda {

class CudaEvent
{
public:
	CudaEvent();
	CudaEvent(const CudaEvent&) = delete;
	CudaEvent(CudaEvent&& orig) noexcept;
	~CudaEvent();

	CudaEvent& operator=(const CudaEvent&) = delete;
	CudaEvent& operator=(CudaEvent&& orig) noexcept;

	cudaEvent_t get() const noexcept;
private:
	cudaEvent_t m_handle = nullptr;
};

}
}
}