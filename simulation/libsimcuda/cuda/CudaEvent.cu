#include "CudaEvent.cuh"
#include "cuda-helpers.cuh"

using namespace networks::simulation::cuda;

CudaEvent::CudaEvent()
{
	CudaSafeCall(cudaEventCreate(&m_handle));
}

CudaEvent::CudaEvent(CudaEvent&& orig) noexcept
{
	*this = std::move(orig);
}

CudaEvent::~CudaEvent()
{
	if (m_handle)
		cudaEventDestroy(m_handle);
}

CudaEvent& CudaEvent::operator=(CudaEvent&& orig) noexcept
{
	std::swap(orig.m_handle, m_handle);
	return *this;
}

cudaEvent_t CudaEvent::get() const noexcept
{
	return m_handle;
}