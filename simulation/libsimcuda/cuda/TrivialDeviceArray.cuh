#pragma once

#include <cuda_runtime.h>
#include <stdexcept>
#include <iostream>
#include <vector>

#include "cuda-helpers.cuh"
#include "CudaStream.cuh"

namespace networks {
namespace simulation {
namespace cuda {

template<typename T>
class TrivialDeviceArray
{
public:
	TrivialDeviceArray() = default;
	TrivialDeviceArray(size_t size);
	TrivialDeviceArray(const std::vector<T>& hostData);
	TrivialDeviceArray(const TrivialDeviceArray&) = delete;
	TrivialDeviceArray(TrivialDeviceArray&& orig) noexcept;
	~TrivialDeviceArray() noexcept;

	TrivialDeviceArray& operator=(const TrivialDeviceArray&) = delete;
	TrivialDeviceArray& operator=(TrivialDeviceArray&& orig) noexcept;

	T* data() const noexcept;
	size_t size() const noexcept;
	size_t capacity() const noexcept;

	void resize(size_t newSize, const CudaStream& stream);
private:
	T* m_deviceMemory = nullptr;
	size_t m_size = 0;
	size_t m_capacity = 0;
};

template<typename T>
TrivialDeviceArray<T>::TrivialDeviceArray(size_t size) : m_size(size), m_capacity(size)
{
	const auto status = cudaMalloc(&m_deviceMemory, size * sizeof(T));
	if (status == cudaErrorMemoryAllocation)
	{
		throw std::bad_alloc();
	}
	else
	{
		CudaSafeCall(status);
	}
}

template<typename T>
TrivialDeviceArray<T>::TrivialDeviceArray(const std::vector<T>& hostData) : TrivialDeviceArray(hostData.size())
{
	CudaSafeCall(cudaMemcpy(m_deviceMemory, hostData.data(), hostData.size() * sizeof(T), cudaMemcpyHostToDevice));
}

template<typename T>
TrivialDeviceArray<T>::TrivialDeviceArray(TrivialDeviceArray&& orig) noexcept
{
	*this = std::move(orig);
}

template<typename T>
TrivialDeviceArray<T>::~TrivialDeviceArray() noexcept
{
	if (m_deviceMemory)
	{
		try
		{
			CudaSafeCall(cudaFree(m_deviceMemory));
		}
		catch (const std::exception& ex)
		{
			std::cout << ex.what() << std::endl;
		}
	}
}

template<typename T>
TrivialDeviceArray<T>& TrivialDeviceArray<T>::operator=(TrivialDeviceArray&& orig) noexcept
{
	std::swap(orig.m_deviceMemory, m_deviceMemory);
	std::swap(orig.m_size, m_size);
	std::swap(orig.m_capacity, m_capacity);
	return *this;
}

template<typename T>
T* TrivialDeviceArray<T>::data() const noexcept
{
	return m_deviceMemory;
}

template<typename T>
size_t TrivialDeviceArray<T>::size() const noexcept
{
	return m_size;
}

template<typename T>
size_t TrivialDeviceArray<T>::capacity() const noexcept
{
	return m_capacity;
}

template<typename T>
void TrivialDeviceArray<T>::resize(size_t newSize, const CudaStream& stream)
{
	if (newSize <= m_capacity)
	{
		m_size = newSize;
	}
	else
	{
		CudaSafeCall(cudaStreamSynchronize(stream.get()));
		*this = TrivialDeviceArray<T>(newSize);
	}
}

}
}
}
