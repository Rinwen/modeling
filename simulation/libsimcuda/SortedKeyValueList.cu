#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <stdexcept>

#include "cuda/cuda-helpers.cuh"
#include "SortedKeyValueList.cuh"

#include <cub/device/device_radix_sort.cuh>

using namespace networks::simulation::cuda;

static __global__ void FillIndicesKernel(const unsigned count, uint32_t* __restrict__ indices)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= count)
		return;

	indices[threadId] = uint32_t(threadId);
}

SortedKeyValueList::SortedKeyValueList(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream) :
	m_cubInternalStorage(cubInternalStorage),
	m_stream(stream)
{
	if (m_cubInternalStorage == nullptr)
		throw std::invalid_argument("empty internal storage");
	if (m_stream == nullptr)
		throw std::invalid_argument("empty stream");
}

void SortedKeyValueList::sort(const TrivialDeviceArray<uint32_t>& keys)
{
	m_indices.resize(keys.size(), *m_stream);
	m_sortedKeys.resize(keys.size(), *m_stream);
	m_sortedIndices.resize(keys.size(), *m_stream);

	const auto d_keysIn = keys.data();
	const auto d_indicesIn = m_indices.data();
	auto d_keysOut = m_sortedKeys.data();
	auto d_indicesOut = m_sortedIndices.data();

	FillIndicesKernel <<<gridSize(unsigned(keys.size())), kBlockSize, 0, m_stream->get() >>> (unsigned(keys.size()), m_indices.data());
	CudaCheckError();

	size_t storageBytes;
	CudaSafeCall(cub::DeviceRadixSort::SortPairs(nullptr, storageBytes, d_keysIn, d_keysOut, d_indicesIn, d_indicesOut, int(keys.size()), 0, sizeof(uint32_t) * 8, m_stream->get()));
	m_cubInternalStorage->resize(storageBytes, *m_stream);
	auto d_storage = m_cubInternalStorage->data();
	CudaSafeCall(cub::DeviceRadixSort::SortPairs(d_storage, storageBytes, d_keysIn, d_keysOut, d_indicesIn, d_indicesOut, int(keys.size()), 0, sizeof(uint32_t) * 8, m_stream->get()));
}

const TrivialDeviceArray<uint32_t>& SortedKeyValueList::indices() const noexcept
{
	return m_sortedIndices;
}