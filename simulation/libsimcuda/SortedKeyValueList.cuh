#pragma once

#include <memory>

#include "cuda/CudaStream.cuh"
#include "cuda/TrivialDeviceArray.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class SortedKeyValueList
{
public:
	SortedKeyValueList() = default;
	explicit SortedKeyValueList(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream);
	SortedKeyValueList(const SortedKeyValueList&) = delete;
	SortedKeyValueList(SortedKeyValueList&&) = default;
	~SortedKeyValueList() = default;

	SortedKeyValueList& operator=(const SortedKeyValueList&) = delete;
	SortedKeyValueList& operator=(SortedKeyValueList&&) = default;

	void sort(const TrivialDeviceArray<uint32_t>& keys);
	const TrivialDeviceArray<uint32_t>& indices() const noexcept;
private:
	std::shared_ptr<TrivialDeviceArray<uint8_t>> m_cubInternalStorage;
	std::shared_ptr<CudaStream> m_stream;

	TrivialDeviceArray<uint32_t> m_indices;
	TrivialDeviceArray<uint32_t> m_sortedKeys;
	TrivialDeviceArray<uint32_t> m_sortedIndices;
};

}
}
}