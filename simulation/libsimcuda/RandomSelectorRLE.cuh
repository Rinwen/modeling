#pragma once

#include <memory>

#include "cuda/CudaStream.cuh"
#include "cuda/TrivialDeviceArray.cuh"

#include "RunLengthEncode.cuh"
#include "ExclusiveScanList.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class RandomSelectorRLE
{
public:
	RandomSelectorRLE() = default;
	explicit RandomSelectorRLE(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream);
	RandomSelectorRLE(const RandomSelectorRLE&) = delete;
	RandomSelectorRLE(RandomSelectorRLE&&) = default;
	~RandomSelectorRLE() = default;

	RandomSelectorRLE& operator=(const RandomSelectorRLE&) = delete;
	RandomSelectorRLE& operator=(RandomSelectorRLE&&) = default;

	void vote(const TrivialDeviceArray<uint32_t>& keys, const uint32_t* randomValues);
	const TrivialDeviceArray<uint32_t>& indices() const noexcept;
private:
	std::shared_ptr<TrivialDeviceArray<uint8_t>> m_cubInternalStorage;
	std::shared_ptr<CudaStream> m_stream;

	TrivialDeviceArray<uint32_t> m_voteIndices;

	RunLengthEncode m_groupBy;
	ExclusiveScanList m_groupStart;
};

}
}
}