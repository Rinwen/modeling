#pragma once
#include <cstdint>

#include <simulation/simulation.hpp>

#include "cuda/CudaStream.cuh"
#include "cuda/TrivialDeviceArray.cuh"

#include "InclusiveScanList.cuh"

namespace networks {
namespace simulation {
namespace cuda {

struct EdgeListSOA
{
	uint32_t* __restrict__ from;
	uint32_t* __restrict__ to;
	float* __restrict__ lambda;
	float* __restrict__ delta;
};

class EdgeListStorage
{
public:
	EdgeListStorage() = default;
	explicit EdgeListStorage(size_t edges, const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream);
	explicit EdgeListStorage(const Matrix<uint8_t>& adjacencyMatrix, const std::vector<size_t>& clusterRanges, const std::vector<SimulationState::ClusterConnection>& clusterConnections, const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream);
	EdgeListStorage(const EdgeListStorage&) = delete;
	EdgeListStorage(EdgeListStorage&&) = default;

	EdgeListStorage& operator= (const EdgeListStorage&) = delete;
	EdgeListStorage& operator= (EdgeListStorage&&) = default;

	//getters
	size_t size() const noexcept;
	EdgeListSOA get() const noexcept;
	const TrivialDeviceArray<uint32_t>& from() const noexcept;
	const TrivialDeviceArray<uint32_t>& to() const noexcept;
	const TrivialDeviceArray<float>& lambda() const noexcept;
	const TrivialDeviceArray<float>& delta() const noexcept;

	//setters
	void resize(size_t edges);
	void filter(const TrivialDeviceArray<int32_t>& state, EdgeListStorage& out);
	void gather(const TrivialDeviceArray<uint32_t>& from, EdgeListStorage& to) const;
private:
	TrivialDeviceArray<uint32_t> m_from;
	TrivialDeviceArray<uint32_t> m_to;
	TrivialDeviceArray<float> m_lambda;
	TrivialDeviceArray<float> m_delta;

	std::shared_ptr<CudaStream> m_stream;
	TrivialDeviceArray<int32_t> m_predicateResult;
	InclusiveScanList m_inclusiveScan;
};

}
}
}