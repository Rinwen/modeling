#include <cub/device/device_run_length_encode.cuh>

#include "RunLengthEncode.cuh"

using namespace networks;
using namespace simulation;
using namespace cuda;

RunLengthEncode::RunLengthEncode(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream) :
	m_cubInternalStorage(cubInternalStorage), 
	m_stream(stream)
{
	if (cubInternalStorage == nullptr)
		throw std::invalid_argument("internal storage is null");

	if (stream == nullptr)
		throw std::invalid_argument("stream storage is null");
}

void RunLengthEncode::rle(const TrivialDeviceArray<uint32_t>& values)
{
	m_unique.resize(values.size(), *m_stream);
	m_counts.resize(values.size(), *m_stream);
	m_numRuns.resize(1, *m_stream);

	size_t storageBytes;
	CudaSafeCall(cub::DeviceRunLengthEncode::Encode(nullptr, storageBytes, values.data(), m_unique.data(), m_counts.data(), m_numRuns.data(), int(values.size()), m_stream->get()));
	m_cubInternalStorage->resize(storageBytes, *m_stream);
	CudaSafeCall(cub::DeviceRunLengthEncode::Encode(m_cubInternalStorage->data(), storageBytes, values.data(), m_unique.data(), m_counts.data(), m_numRuns.data(), int(values.size()), m_stream->get()));
}

const TrivialDeviceArray<uint32_t>& RunLengthEncode::unique() const noexcept
{
	return m_unique;
}

const TrivialDeviceArray<uint32_t>& RunLengthEncode::counts() const noexcept
{
	return m_counts;
}

const uint32_t* RunLengthEncode::numRuns() const noexcept
{
	return m_numRuns.data();
}