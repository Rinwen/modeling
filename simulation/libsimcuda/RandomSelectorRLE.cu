#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <stdexcept>
#include <string>

#include "cuda/cuda-helpers.cuh"

#include "RandomSelectorRLE.cuh"

using namespace networks::simulation::cuda;

static __global__ void RandomSelectorKernel(
	unsigned keysCount,
	const uint32_t* __restrict__ uniqueValuesCountPtr,
	const uint32_t* __restrict__ uniqueValues,
	const uint32_t* __restrict__ uniqueGroupSize,
	const int32_t* __restrict__ uniqueGroupStart,
	const uint32_t* __restrict__ randomResults,
	uint32_t* __restrict__ indices)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= keysCount)
		return;

	auto uniqueValuesCount = *uniqueValuesCountPtr;
	if (uniqueValues[uniqueValuesCount - 1] == uint32_t(-1))
		uniqueValuesCount--;

	auto indexToSelect = uint32_t(-1);

	if (threadId < uniqueValuesCount)
	{
		const auto groupSize = uniqueGroupSize[threadId];
		const auto randValue = randomResults[threadId];
		const auto groupStart = uniqueGroupStart[threadId];
		indexToSelect = groupStart + randValue % groupSize;
	}

	indices[threadId] = indexToSelect;
}

RandomSelectorRLE::RandomSelectorRLE(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream) :
	m_cubInternalStorage(cubInternalStorage),
	m_stream(stream),
	m_groupBy(cubInternalStorage, stream),
	m_groupStart(cubInternalStorage, stream)
{
	if (m_cubInternalStorage == nullptr)
		throw std::invalid_argument("empty internal storage");

	if (m_stream == nullptr)
		throw std::invalid_argument("empty stream");
}

void RandomSelectorRLE::vote(const TrivialDeviceArray<uint32_t>& keys, const uint32_t* randomValues)
{
	m_voteIndices.resize(keys.size(), *m_stream);

	m_groupBy.rle(keys);
	m_groupStart.scan(m_groupBy.counts());

	RandomSelectorKernel <<<gridSize(unsigned(keys.size())), kBlockSize, 0, m_stream->get() >>> (
		unsigned(keys.size()),
		m_groupBy.numRuns(),
		m_groupBy.unique().data(),
		m_groupBy.counts().data(),
		m_groupStart.result().data(),
		randomValues,
		m_voteIndices.data());
	CudaCheckError();
}

const TrivialDeviceArray<uint32_t>& RandomSelectorRLE::indices() const noexcept
{
	return m_voteIndices;
}