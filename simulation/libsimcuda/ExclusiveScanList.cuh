#pragma once

#include <cstdint>
#include <memory>

#include "cuda/CudaStream.cuh"
#include "cuda/TrivialDeviceArray.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class ExclusiveScanList
{
public:
	ExclusiveScanList() = default;
	explicit ExclusiveScanList(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream);
	ExclusiveScanList(const ExclusiveScanList&) = delete;
	ExclusiveScanList(ExclusiveScanList&&) = default;
	~ExclusiveScanList() = default;

	ExclusiveScanList& operator=(const ExclusiveScanList&) = delete;
	ExclusiveScanList& operator=(ExclusiveScanList&&) = default;

	void scan(const TrivialDeviceArray<int8_t>& values);
	void scan(const TrivialDeviceArray<uint32_t>& values);
	const TrivialDeviceArray<int32_t>& result() const noexcept;
private:
	std::shared_ptr<TrivialDeviceArray<uint8_t>> m_cubInternalStorage;
	std::shared_ptr<CudaStream> m_stream;
	TrivialDeviceArray<int32_t> m_result;
};

}
}
}