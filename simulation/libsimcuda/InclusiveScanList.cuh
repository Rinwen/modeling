#pragma once

#include <cstdint>
#include <memory>

#include "cuda/CudaStream.cuh"
#include "cuda/TrivialDeviceArray.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class InclusiveScanList
{
public:
	InclusiveScanList() = default;
	explicit InclusiveScanList(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream);
	InclusiveScanList(const InclusiveScanList&) = delete;
	InclusiveScanList(InclusiveScanList&&) = default;
	~InclusiveScanList() = default;

	InclusiveScanList& operator=(const InclusiveScanList&) = delete;
	InclusiveScanList& operator=(InclusiveScanList&&) = default;

	void scan(const TrivialDeviceArray<int32_t>& values);
	const TrivialDeviceArray<int32_t>& result() const noexcept;
private:
	std::shared_ptr<TrivialDeviceArray<uint8_t>> m_cubInternalStorage;
	std::shared_ptr<CudaStream> m_stream;
	TrivialDeviceArray<int32_t> m_result;
	size_t m_lastSize = 0;
};

}
}
}