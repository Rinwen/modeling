#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <algorithm>
#include <stdexcept>
#include <string>

#include "cuda/cuda-helpers.cuh"
#include "EdgeListStorage.cuh"

using namespace networks::simulation::cuda;

static __global__ void PredicateKernel(const unsigned edges, const uint32_t* __restrict__ from, const uint32_t* __restrict__ to, const int32_t* __restrict__ state, int32_t* __restrict__ predicateResult)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= edges)
		return;

	const auto f = from[threadId];
	const auto t = to[threadId];
	auto result = int32_t(0);

	if (state[f] && !state[t])
		result = 1;

	predicateResult[threadId] = result;
}

static __global__ void ScatterKernel(const unsigned edges, EdgeListSOA from, const int32_t* __restrict__ predicateResult, const int32_t* __restrict__ scanResult, EdgeListSOA to)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= edges)
		return;

	const auto totalCount = scanResult[edges - 1];

	if (threadId >= totalCount)
	{
		to.from[threadId] = uint32_t(-1);
		to.to[threadId] = uint32_t(-1);
	}

	if (predicateResult[threadId])
	{
		const auto outIdx = scanResult[threadId] - 1;

		to.from[outIdx] = from.from[threadId];
		to.to[outIdx] = from.to[threadId];
		to.lambda[outIdx] = from.lambda[threadId];
		to.delta[outIdx] = from.delta[threadId];
	}
}

static __global__ void GatherKernel(const unsigned finalEdges, EdgeListSOA from, const uint32_t* __restrict__ sources, EdgeListSOA to)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= finalEdges)
		return;

	const auto sourceIdx = sources[threadId];

	auto fromIdx = uint32_t(-1);
	auto toIdx = uint32_t(-1);
	auto lambda = 0.0f;
	auto delta = 0.0f;

	if (sourceIdx != uint32_t(-1))
	{
		fromIdx = from.from[sourceIdx];
		toIdx = from.to[sourceIdx];
		lambda = from.lambda[sourceIdx];
		delta = from.delta[sourceIdx];
	}

	to.from[threadId] = fromIdx;
	to.to[threadId] = toIdx;
	to.lambda[threadId] = lambda;
	to.delta[threadId] = delta;
}

struct Edge
{
	uint32_t from;
	uint32_t to;
	float lambda;
	float delta;
};

static size_t getCluster(const std::vector<size_t>& clusterRanges, size_t vertexIndex)
{
	auto it = std::find_if(clusterRanges.cbegin(), clusterRanges.cend(), [&](size_t clusterEnd)
	{
		return vertexIndex < clusterEnd;
	});

	return std::distance(clusterRanges.cbegin(), it);
}

EdgeListStorage::EdgeListStorage(size_t size, const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream) :
	m_stream(stream),
	m_inclusiveScan(cubInternalStorage, stream)
{
	if (cubInternalStorage == nullptr)
		throw std::invalid_argument("internal storage is null");

	if (stream == nullptr)
		throw std::invalid_argument("stream storage is null");

	resize(size);
}

EdgeListStorage::EdgeListStorage(const Matrix<uint8_t>& adjacencyMatrix,
	const std::vector<size_t>& clusterRanges,
	const std::vector<SimulationState::ClusterConnection>& clusterConnections,
	const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage,
	const std::shared_ptr<CudaStream>& stream) :
	m_stream(stream),
	m_inclusiveScan(cubInternalStorage, stream)
{
	if (cubInternalStorage == nullptr)
		throw std::invalid_argument("internal storage is null");

	if (stream == nullptr)
		throw std::invalid_argument("stream storage is null");
	auto edges = std::vector<Edge>();

	for (size_t row = 0; row < adjacencyMatrix.rows(); ++row)
	{
		for (size_t column = 0; column < adjacencyMatrix.columns(); ++column)
		{
			if (!adjacencyMatrix.at(row, column))
				continue;

			edges.emplace_back(Edge{ uint32_t(row), uint32_t(column), 0.0f, 0.0f });
		}
	}

	std::sort(edges.begin(), edges.end(), [](const Edge& lhs, const Edge& rhs)
	{
		return lhs.from < rhs.from;
	});

	for (auto& edge : edges)
	{
		auto fromCluster = getCluster(clusterRanges, edge.from);
		auto toCluster = getCluster(clusterRanges, edge.to);
		auto it = std::find_if(clusterConnections.cbegin(), clusterConnections.cend(), [&](const SimulationState::ClusterConnection& connection)
		{
			return connection.from == fromCluster && connection.to == toCluster;
		});

		if (it == clusterConnections.cend())
			throw std::invalid_argument("unknown lambda between clusters " + std::to_string(fromCluster) + " and " + std::to_string(toCluster));
		
		edge.lambda = it->lambda;
		edge.delta = it->delta;
	}

	auto from = std::vector<uint32_t>(edges.size());
	auto to = std::vector<uint32_t>(edges.size());
	auto lambda = std::vector<float>(edges.size());
	auto delta = std::vector<float>(edges.size());

	for (size_t i = 0; i < edges.size(); ++i)
	{
		from[i] = edges[i].from;
		to[i] = edges[i].to;
		lambda[i] = edges[i].lambda;
		delta[i] = edges[i].delta;
	}

	m_from = from;
	m_to = to;
	m_lambda = lambda;
	m_delta = delta;
}

size_t EdgeListStorage::size() const noexcept
{
	return m_from.size();
}

EdgeListSOA EdgeListStorage::get() const noexcept
{
	auto self = const_cast<EdgeListStorage*>(this);

	return
	{
		self->m_from.data(),
		self->m_to.data(),
		self->m_lambda.data(),
		self->m_delta.data()
	};
}

const TrivialDeviceArray<uint32_t>& EdgeListStorage::from() const noexcept
{
	return m_from;
}

const TrivialDeviceArray<uint32_t>& EdgeListStorage::to() const noexcept
{
	return m_to;
}

const TrivialDeviceArray<float>& EdgeListStorage::lambda() const noexcept
{
	return m_lambda;
}

const TrivialDeviceArray<float>& EdgeListStorage::delta() const noexcept
{
	return m_delta;
}

void EdgeListStorage::resize(size_t size)
{
	m_from.resize(size, *m_stream);
	m_to.resize(size, *m_stream);
	m_lambda.resize(size, *m_stream);
	m_delta.resize(size, *m_stream);
}

void EdgeListStorage::filter(const TrivialDeviceArray<int32_t>& state, EdgeListStorage& out)
{
	m_predicateResult.resize(size(), *m_stream);
	out.resize(size());
	
	const auto edges = unsigned(size());

	PredicateKernel <<<gridSize(edges), kBlockSize, 0, m_stream->get()>>> (edges, m_from.data(), m_to.data(), state.data(), m_predicateResult.data());
	CudaCheckError();

	m_inclusiveScan.scan(m_predicateResult);

	ScatterKernel <<<gridSize(edges), kBlockSize, 0, m_stream->get() >>> (edges, get(), m_predicateResult.data(), m_inclusiveScan.result().data(), out.get());
	CudaCheckError();
};

void EdgeListStorage::gather(const TrivialDeviceArray<uint32_t>& from, EdgeListStorage& to) const
{
	to.resize(from.size());

	GatherKernel<<<gridSize(unsigned(from.size())), kBlockSize, 0, m_stream->get()>>>(unsigned(from.size()), get(), from.data(), to.get());
	CudaCheckError();
}