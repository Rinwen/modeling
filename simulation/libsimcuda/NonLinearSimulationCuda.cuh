#pragma once

#include <cuda_runtime.h>
#include <cstdint>

#include <array>
#include <chrono>
#include <memory>

#include <simulation/simulation.hpp>

#include "cuda/CudaStream.cuh"
#include "cuda/CudaEvent.cuh"
#include "cuda/TrivialDeviceArray.cuh"
#include "cuda/TrivialHostArray.cuh"
#include "cuda/curand/CurandGenerator.cuh"

#include "EdgeListStorage.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class NonLinearSimulationCuda : public ISimulation
{
public:
	explicit NonLinearSimulationCuda(SimulationState&& state, const std::shared_ptr<networks::IEventLoop>& eventLoop);
	virtual ~NonLinearSimulationCuda() override = default;

	virtual std::pair<const float*, const float*> update() override;
private:
	static constexpr size_t kBatchSize = 4096;
	static constexpr size_t kRandomSteps = 4096;

	std::vector<size_t> m_clusters;
	float m_mu = 0.0f;
	float m_gamma = 0.0f;
	unsigned m_thresholdNumber = 0;
	size_t m_randomStep = 0;

	TrivialDeviceArray<int32_t> m_nodesState;
	TrivialDeviceArray<int8_t> m_stateDerivative;
	TrivialDeviceArray<int32_t> m_nextState;
	TrivialDeviceArray<unsigned> m_atomicCounters;
	TrivialDeviceArray<uint32_t> m_randomValues;

	std::shared_ptr<CudaStream> m_stream = std::make_shared<CudaStream>();
	
	EdgeListStorage m_edgeList;
	CurandGenerator m_generator{ std::chrono::high_resolution_clock::now().time_since_epoch().count(), m_stream };
	std::shared_ptr<TrivialDeviceArray<uint8_t>> m_cubInternalStorage = std::make_shared<TrivialDeviceArray<uint8_t>>();

	InclusiveScanList m_activityScan;
	TrivialDeviceArray<int32_t> m_deviceActivityBatch;
	TrivialHostArray<int32_t> m_hostActivityScanBatch;
	std::vector<float> m_statsBuffer;
	std::shared_ptr<networks::IEventLoop> m_eventLoop;

	void countActiveNeighbors();
	void randomActivationAndExtinguishing();
	void transmitActivity();
	void updateNextState(size_t batchId);
	void downloadNextState();
	void updateStats();
	void postProcess();
};

}
}
}