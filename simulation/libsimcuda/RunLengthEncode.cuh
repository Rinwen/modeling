#pragma once

#include <cstdint>
#include <memory>

#include "cuda/CudaStream.cuh"
#include "cuda/TrivialDeviceArray.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class RunLengthEncode
{
public:
	RunLengthEncode() = default;
	explicit RunLengthEncode(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream);
	RunLengthEncode(const RunLengthEncode&) = delete;
	RunLengthEncode(RunLengthEncode&&) = default;
	~RunLengthEncode() = default;

	RunLengthEncode& operator=(const RunLengthEncode&) = delete;
	RunLengthEncode& operator=(RunLengthEncode&&) = default;

	void rle(const TrivialDeviceArray<uint32_t>& values);
	const TrivialDeviceArray<uint32_t>& unique() const noexcept;
	const TrivialDeviceArray<uint32_t>& counts() const noexcept;
	const uint32_t* numRuns() const noexcept;
private:
	std::shared_ptr<TrivialDeviceArray<uint8_t>> m_cubInternalStorage;
	std::shared_ptr<CudaStream> m_stream;

	TrivialDeviceArray<uint32_t> m_unique;
	TrivialDeviceArray<uint32_t> m_counts;
	TrivialDeviceArray<uint32_t> m_numRuns;
};

}
}
}