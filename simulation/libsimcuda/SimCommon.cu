#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "SimCommon.cuh"


namespace networks {
namespace simulation {
namespace cuda {

std::vector<size_t> makeClusters(const std::vector<size_t>& clusterSizes)
{
	auto clusters = std::vector<size_t>(clusterSizes.size());
	for (size_t i = 0, incScan = 0; i < clusterSizes.size(); ++i)
	{
		incScan += clusterSizes[i];
		clusters[i] = incScan;
	}

	return clusters;
}

std::pair<size_t, size_t> clusterRange(const std::vector<size_t>& clusters, size_t idx)
{
	if (idx == 0)
	{
		return std::make_pair(size_t(0), clusters[idx]);
	}
	else
	{
		return std::make_pair(clusters[idx - 1], clusters[idx]);
	}
};

TrivialDeviceArray<int32_t> makeInitialState(const std::vector<size_t>& clusters, size_t hotCluster)
{
	auto state = std::vector<int32_t>(clusters.back(), 0);
	auto hotClusterRange = clusterRange(clusters, hotCluster);
	for (size_t i = hotClusterRange.first; i != hotClusterRange.second; ++i)
		state[i] = 1;

	return state;
}

__global__ void NextStateKernel(const unsigned nodesCount, const int32_t* __restrict__ state, const int8_t* __restrict__ derivative, int32_t* __restrict__ nextState)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= nodesCount)
		return;

	nextState[threadId] = state[threadId] + derivative[threadId];
}

__global__ void CopyDataKernel(const unsigned elements, const int32_t* __restrict__ from, int32_t* __restrict__ to)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= elements)
		return;

	to[threadId] = from[threadId];
}

}
}
}