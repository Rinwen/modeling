#include <cub/device/device_scan.cuh>

#include "InclusiveScanList.cuh"

using namespace networks;
using namespace simulation;
using namespace cuda;

InclusiveScanList::InclusiveScanList(const std::shared_ptr<TrivialDeviceArray<uint8_t>>& cubInternalStorage, const std::shared_ptr<CudaStream>& stream) :
	m_cubInternalStorage(cubInternalStorage), 
	m_stream(stream)
{
	if (cubInternalStorage == nullptr)
		throw std::invalid_argument("internal storage is null");

	if (stream == nullptr)
		throw std::invalid_argument("stream storage is null");
}
void InclusiveScanList::scan(const TrivialDeviceArray<int32_t>& values)
{
	m_result.resize(values.size(), *m_stream);

	if (m_lastSize < values.size())
	{
		auto storageBytes = size_t(0);
		CudaSafeCall(cub::DeviceScan::InclusiveSum(nullptr, storageBytes, values.data(), m_result.data(), int(values.size()), m_stream->get()));
		m_cubInternalStorage->resize(storageBytes, *m_stream);
		m_lastSize = values.size();
	}

	auto storageBytes = m_cubInternalStorage->capacity();

	CudaSafeCall(cub::DeviceScan::InclusiveSum(m_cubInternalStorage->data(), storageBytes, values.data(), m_result.data(), int(values.size()), m_stream->get()));
}
const TrivialDeviceArray<int32_t>& InclusiveScanList::result() const noexcept
{
	return m_result;
}