#pragma once

#include <cuda_runtime.h>
#include <cstdint>

#include <array>
#include <chrono>
#include <memory>

#include <simulation/simulation.hpp>
#include "cuda/CudaStream.cuh"
#include "cuda/CudaEvent.cuh"
#include "cuda/TrivialDeviceArray.cuh"
#include "cuda/TrivialHostArray.cuh"
#include "cuda/curand/CurandGenerator.cuh"

#include "EdgeListStorage.cuh"
#include "RandomSelectorRLE.cuh"
#include "SortedKeyValueList.cuh"
#include "InclusiveScanList.cuh"

namespace networks {
namespace simulation {
namespace cuda {

class LinearSimulationCuda : public ISimulation
{
public:
	explicit LinearSimulationCuda(SimulationState&& state);
	virtual ~LinearSimulationCuda() override = default;

	virtual std::pair<const float*, const float*> update() override;
private:
	static constexpr size_t kBatchSize = 512;
	static constexpr size_t kRandomSteps = 512;

	std::vector<size_t> m_clusters;
	SimulationState::Waveform m_waveform = SimulationState::Waveform::None;
	int m_period = 0;
	float m_mu = 0.0f;
	size_t m_randomStep = 0;
	size_t m_batchWriteIdx = 0;

	std::shared_ptr<CudaStream> m_stream = std::make_shared<CudaStream>();
	std::shared_ptr<TrivialDeviceArray<uint8_t>> m_cubInternalStorage = std::make_shared<TrivialDeviceArray<uint8_t>>();

	TrivialDeviceArray<unsigned long long> m_step = TrivialDeviceArray<unsigned long long>(std::vector<unsigned long long>{ 0 });
	TrivialDeviceArray<int32_t> m_nodesState;
	TrivialDeviceArray<int8_t> m_stateDerivative;
	TrivialDeviceArray<int32_t> m_nextState;
	TrivialDeviceArray<uint32_t> m_randomValues;

	CurandGenerator m_generator{ std::chrono::high_resolution_clock::now().time_since_epoch().count(), m_stream };

	EdgeListStorage m_edgeList;
	EdgeListStorage m_activeToInactives;
	EdgeListStorage m_activeToSingleInactive;
	EdgeListStorage m_activeToSingleInactiveSorted;
	EdgeListStorage m_singleActiveToSingleInactive;
	RandomSelectorRLE m_vote;
	SortedKeyValueList m_sorted;

	std::vector<float> m_statsBuffer;
	InclusiveScanList m_activityScan;
	TrivialDeviceArray<int32_t> m_deviceActivityBatch;
	TrivialHostArray<int32_t> m_hostActivityScanBatch;
	CudaEvent m_activityScanCompleteEvent;
	CudaEvent m_downloadEvent;
	CudaStream m_downloadStream;

	void edgeSelection();
	void computeDerivative();
	void updateNextState();
	void updateStats();
	void downloadNextState();
};

}
}
}