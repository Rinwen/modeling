#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <future>

#include <libio/IEventLoop.hpp>

#include "cuda/cuda-helpers.cuh"
#include "SimCommon.cuh"
#include "NonLinearSimulationCuda.cuh"

using namespace networks::simulation;
using namespace networks::simulation::cuda;

static __global__ void ResetAtomicCounters(const unsigned nodesCount, unsigned* __restrict__ atomicCounters)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= nodesCount)
		return;

	atomicCounters[threadId] = 0;
}

static __global__ void CountActiveNeighborsKernel(const unsigned edgesCount, const EdgeListSOA edges, const int32_t* __restrict__ nodesState, unsigned* __restrict__ atomicCounters)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= edgesCount)
		return;

	if (!nodesState[edges.from[threadId]])
		return;

	const auto toIdx = edges.to[threadId];
	if (nodesState[toIdx])
		return;

	atomicAdd(atomicCounters + toIdx, 1);
}

static __global__ void RandomStateChangerKernel(
	const unsigned nodesCount,
	const float mu,
	const float gamma,
	const int32_t* __restrict__ nodesState,
	const uint32_t* __restrict__ randomValues,
	int8_t* __restrict__ stateDerivative)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= nodesCount)
		return;

	const auto random = float(randomValues[threadId]) / float(0xFFFFFFFF);
	int8_t derivative = 0;


	if (nodesState[threadId])
	{
		if (random < mu)
		{
			derivative = -1;
		}
	}
	else
	{
		if (random < gamma)
		{
			derivative = 1;
		}
	}

	__syncwarp();

	stateDerivative[threadId] = derivative;
}

static __global__ void TransmitActivityKernel(
	const unsigned nodesCount,
	const unsigned thresholdNumber,
	const int32_t* __restrict__ nodesState,
	const unsigned* __restrict__ atomicCounters,
	int8_t* __restrict__ stateDerivative)
{
	const auto threadId = blockIdx.x * blockDim.x + threadIdx.x;
	if (threadId >= nodesCount)
		return;

	if (nodesState[threadId])
		return;

	if (atomicCounters[threadId] < thresholdNumber)
		return;

	stateDerivative[threadId] = 1;
}

NonLinearSimulationCuda::NonLinearSimulationCuda(SimulationState&& state_, const std::shared_ptr<networks::IEventLoop>& eventLoop)
{
	const auto state = std::move(state_);

	m_clusters = makeClusters(state.clusterSizes);
	m_mu = state.mu;
	m_gamma = state.gamma;
	m_thresholdNumber = state.thresholdNumber;

	m_nodesState = makeInitialState(m_clusters, state.hotCluster);
	m_stateDerivative.resize(m_nodesState.size(), *m_stream);
	m_nextState.resize(m_nodesState.size(), *m_stream);
	m_atomicCounters.resize(m_nodesState.size(), *m_stream);

	m_edgeList = EdgeListStorage(state.adjacencyMatrix, m_clusters, state.clusterConnections, m_cubInternalStorage, m_stream);

	m_activityScan = InclusiveScanList(m_cubInternalStorage, m_stream);
	m_deviceActivityBatch.resize(m_nodesState.size() * kBatchSize, *m_stream);
	m_hostActivityScanBatch.resize(m_nodesState.size() * kBatchSize);
	
	m_eventLoop = eventLoop;
}

std::pair<const float*, const float*> NonLinearSimulationCuda::update()
{
	const auto statsSize = m_clusters.size() + 1;
	if (!m_statsBuffer.empty())
	{
		// Check if the network never fires up again.
		if (m_gamma == 0.0f && m_statsBuffer[statsSize - 1] == 0)
		{
			// returns zeroes
			return std::make_pair(m_statsBuffer.data(), m_statsBuffer.data() + statsSize);
		}
		m_statsBuffer.erase(m_statsBuffer.begin(), m_statsBuffer.begin() + statsSize);
	}

	if (m_statsBuffer.empty())
	{
		for (size_t i = 0; i < kBatchSize; ++i)
		{
			m_eventLoop->enqueue(std::bind(&NonLinearSimulationCuda::countActiveNeighbors, this));
			m_eventLoop->enqueue(std::bind(&NonLinearSimulationCuda::randomActivationAndExtinguishing, this));
			m_eventLoop->enqueue(std::bind(&NonLinearSimulationCuda::transmitActivity, this));
			m_eventLoop->enqueue(std::bind(&NonLinearSimulationCuda::updateNextState, this, i));
			m_eventLoop->enqueue(std::bind(&NonLinearSimulationCuda::postProcess, this));
		}
		m_eventLoop->enqueue(std::bind(&NonLinearSimulationCuda::downloadNextState, this));
		
		auto promise = std::promise<void>();
		m_eventLoop->enqueue([&]
		{
			CudaSafeCall(cudaStreamSynchronize(m_stream->get()));
			promise.set_value();
		});
		promise.get_future().get();
		updateStats();
	}

	return std::make_pair(m_statsBuffer.data(), m_statsBuffer.data() + statsSize);
}

void NonLinearSimulationCuda::countActiveNeighbors()
{
	const auto nodesCount = unsigned(m_nodesState.size());
	ResetAtomicCounters <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get() >>> (nodesCount, m_atomicCounters.data());
	CudaCheckError();

	const auto edgesCount = unsigned(m_edgeList.size());
	CountActiveNeighborsKernel <<<gridSize(edgesCount), kBlockSize, 0, m_stream->get()>>> (edgesCount, m_edgeList.get(), m_nodesState.data(), m_atomicCounters.data());
	CudaCheckError();
}

void NonLinearSimulationCuda::transmitActivity()
{
	const auto nodesCount = unsigned(m_nodesState.size());
	TransmitActivityKernel <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get()>>> (
		nodesCount,
		m_thresholdNumber,
		m_nodesState.data(),
		m_atomicCounters.data(),
		m_stateDerivative.data());
	CudaCheckError();
}

void NonLinearSimulationCuda::updateNextState(size_t batchId)
{
	const auto nodesCount = unsigned(m_nodesState.size());

	NextStateKernel <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get() >>> (nodesCount, m_nodesState.data(), m_stateDerivative.data(), m_nextState.data());
	CudaCheckError();
	m_activityScan.scan(m_nextState);
	//CopyDataKernel <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get() >>> (nodesCount, m_activityScan.result().data(), m_deviceActivityBatch.data() + batchId * nodesCount);
	//CudaCheckError();
	CudaSafeCall(cudaMemcpyAsync(m_deviceActivityBatch.data() + batchId * nodesCount, m_activityScan.result().data(), nodesCount * sizeof(int32_t), cudaMemcpyDeviceToDevice, m_stream->get()));
}

void NonLinearSimulationCuda::downloadNextState()
{
	const auto nodesCount = m_nodesState.size();
	const auto batchBytesSize = nodesCount * sizeof(int32_t) * kBatchSize;

	CudaSafeCall(cudaMemcpyAsync(m_hostActivityScanBatch.data(), m_deviceActivityBatch.data(), batchBytesSize, cudaMemcpyDeviceToHost, m_stream->get()));
}

void NonLinearSimulationCuda::updateStats()
{
	const auto nodesCount = m_nodesState.size();
	const auto h_scanBatch = m_hostActivityScanBatch.data();

	for (size_t scanIdx = 0; scanIdx < kBatchSize; ++scanIdx)
	{
		const auto h_scan = h_scanBatch + scanIdx * nodesCount;
		const auto activeNodes = h_scan[nodesCount - 1];
		
		//if (activeNodes == 0 && m_gamma != 0.0f)
		//	continue;

		for (size_t i = 0; i < m_clusters.size(); ++i)
		{
			const auto range = clusterRange(m_clusters, i);
			auto scan1 = uint32_t(0);
			auto scan2 = uint32_t(0);

			if (range.first != 0)
				scan1 = h_scan[range.first - 1];
			scan2 = h_scan[range.second - 1];

			m_statsBuffer.emplace_back(float(scan2 - scan1) / float(range.second - range.first));
		}
		m_statsBuffer.emplace_back(float(activeNodes) / float(m_clusters.back()));
	}
}

void NonLinearSimulationCuda::randomActivationAndExtinguishing()
{
	const auto nodesCount = unsigned(m_nodesState.size());
	if (m_randomStep == 0)
	{
		m_generator.generate(m_randomValues, kRandomSteps * nodesCount);
	}

	const auto gammaRandomValues = m_randomValues.data() + nodesCount * m_randomStep;
	RandomStateChangerKernel <<<gridSize(nodesCount), kBlockSize, 0, m_stream->get() >>> (
		nodesCount,
		m_mu,
		m_gamma,
		m_nodesState.data(),
		gammaRandomValues,
		m_stateDerivative.data());
	CudaCheckError();
}

void NonLinearSimulationCuda::postProcess()
{
	std::swap(m_nextState, m_nodesState);

	m_randomStep++;
	m_randomStep = m_randomStep % kRandomSteps;
}
