#pragma once

#include <cstddef>
#include <vector>

#include "cuda/TrivialDeviceArray.cuh"


namespace networks {
namespace simulation {
namespace cuda {

std::vector<size_t> makeClusters(const std::vector<size_t>& clusterSizes);

std::pair<size_t, size_t> clusterRange(const std::vector<size_t>& clusters, size_t idx);

TrivialDeviceArray<int32_t> makeInitialState(const std::vector<size_t>& clusters, size_t hotCluster);

__global__ void NextStateKernel(const unsigned nodesCount, const int32_t* __restrict__ state, const int8_t* __restrict__ derivative, int32_t* __restrict__ nextState);
__global__ void CopyDataKernel(const unsigned elements, const int32_t* __restrict__ from, int32_t* __restrict__ to);

}
}
}