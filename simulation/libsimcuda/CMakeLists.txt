cmake_minimum_required(VERSION 3.9)

project(libsimcuda LANGUAGES CXX CUDA)
find_package(CUDAToolkit REQUIRED)

set(SRC
	cuda/curand/CurandGenerator.cu
	cuda/curand/CurandGenerator.cuh

	cuda/cuda-helpers.cuh
	cuda/CudaEvent.cu
	cuda/CudaEvent.cuh
	cuda/CudaStream.cu
	cuda/CudaStream.cuh
	cuda/TrivialDeviceArray.cuh
	cuda/TrivialHostArray.cuh

	EdgeListStorage.cu
	EdgeListStorage.cuh
	ExclusiveScanList.cu
	ExclusiveScanList.cuh
	InclusiveScanList.cu
	InclusiveScanList.cuh
	LinearSimulationCuda.cu
	LinearSimulationCuda.cuh
	NonLinearSimulationCuda.cu
	NonLinearSimulationCuda.cuh
	RandomSelectorRLE.cu
	RandomSelectorRLE.cuh
	RunLengthEncode.cu
	RunLengthEncode.cuh
	SimCommon.cu
	SimCommon.cuh
	SimulationFactory.cu
	SortedKeyValueList.cu
	SortedKeyValueList.cuh

	pch.cuh
)

source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}" FILES ${SRC})

add_library(${PROJECT_NAME} SHARED ${SRC})
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 14)
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD_REQUIRED true)
set_property(TARGET ${PROJECT_NAME} PROPERTY VS_JUST_MY_CODE_DEBUGGING true)
set_property(TARGET ${PROJECT_NAME} PROPERTY CUDA_ARCHITECTURES 61-real 61-virtual)
set_property(TARGET ${PROJECT_NAME} PROPERTY FOLDER simulation)

target_precompile_headers(${PROJECT_NAME} PRIVATE pch.cuh)
target_link_libraries(${PROJECT_NAME} PUBLIC libio)
target_link_libraries(${PROJECT_NAME} PRIVATE CUDA::cudart CUDA::curand)

target_compile_definitions(${PROJECT_NAME} PRIVATE SIMULATION_IMPL)
target_compile_definitions(${PROJECT_NAME} PRIVATE _USE_MATH_DEFINES)

install(TARGETS ${PROJECT_NAME} CONFIGURATIONS Release RUNTIME DESTINATION bin)
install(DIRECTORY ${CUDAToolkit_BIN_DIR} DESTINATION . CONFIGURATIONS Release FILES_MATCHING REGEX "(curand.*.dll)|(cudart64.*.dll)" PATTERN "crt" EXCLUDE)