#include <memory>

#include "LinearSimulationCuda.cuh"
#include "NonLinearSimulationCuda.cuh"

namespace networks {
namespace simulation {
namespace cuda{

std::unique_ptr<ISimulation> networks::simulation::cuda::CreateSimulation(SimulationState&& state, ModelType type, const std::shared_ptr<networks::IEventLoop>& eventLoop)
{
	switch (type)
	{
	case ModelType::LinearActivation:
		throw std::invalid_argument("linear simulation requires additional testing");
		//return std::make_unique<LinearSimulationCuda>(std::move(state));
		break;
	case ModelType::NonLinearActivation:
		return std::make_unique<NonLinearSimulationCuda>(std::move(state), eventLoop);
		break;
	}

	throw std::runtime_error("unknown type");
}

}
}
}
