#pragma once

#include <algorithm>
#include <stdexcept>
#include <vector>

#if __cplusplus >= 201703L 
#include <execution>
#define TRY_PARALLELIZE std::execution::par_unseq,
#else
#define TRY_PARALLELIZE
#endif


namespace networks {

template <typename T>
class Matrix
{
public:
	explicit Matrix(size_t rows, size_t columns);
	explicit Matrix(size_t rows, size_t columns, std::vector<T>&& values);

	template <typename U>
	explicit Matrix(size_t rows, size_t columns, const std::vector<U>& values);

	template <typename U>
	explicit Matrix(const Matrix<U>&);

	Matrix(const Matrix&) = default;
	Matrix(Matrix&&) = default;
	~Matrix() = default;

	template <typename U>
	Matrix<T>& operator= (const Matrix<U>&);
	Matrix& operator= (const Matrix&) = default;
	Matrix& operator= (Matrix&&) = default;

	size_t rows() const noexcept;
	size_t columns() const noexcept;
	const T& at(size_t i, size_t j) const;
	T& at(size_t i, size_t j);

	Matrix transpose() const;
	const std::vector<T>& data() const & noexcept;
	std::vector<T> data() && noexcept;

private:
	std::vector<T> m_values;
	size_t m_rows;
	size_t m_columns;
};

template<typename T>
Matrix<T>::Matrix(size_t rows, size_t columns) : m_rows(rows), m_columns(columns)
{
	if (rows == 0 || columns == 0)
		throw std::invalid_argument("matrix dimension is zero");

	m_values.resize(rows * columns);
}

template<typename T>
Matrix<T>::Matrix(size_t rows, size_t columns, std::vector<T>&& values) : m_rows(rows), m_columns(columns)
{
	if (rows == 0 || columns == 0)
		throw std::invalid_argument("matrix dimension is zero");

	if (values.size() != rows * columns)
		throw std::invalid_argument("invalid values dimensions");

	m_values = std::move(values);
}

template<typename T>
template<typename U>
Matrix<T>::Matrix(size_t rows, size_t columns, const std::vector<U>& values) : Matrix(rows, columns)
{
	if (rows * columns != values.size())
		throw std::invalid_argument("incorrect matrix values vector size");

	std::transform(TRY_PARALLELIZE values.cbegin(), values.cend(), m_values.begin(), [](const auto& v) { return static_cast<T>(v); });
}

template<typename T>
template<typename U>
Matrix<T>::Matrix(const Matrix<U>& m) : Matrix(m.rows(), m.columns())
{
	std::transform(TRY_PARALLELIZE m.data().cbegin(), m.data().cend(), m_values.begin(), [](const auto& v) { return static_cast<T>(v); });
}

template<typename T>
template<typename U>
Matrix<T>& Matrix<T>::operator= (const Matrix<U>& m)
{
	m_rows = m.m_rows;
	m_columns = m.m_columns;
	m_values.resize(m.m_values.size());
	std::transform(TRY_PARALLELIZE m.data().cbegin(), m.data().cend(), m_values.begin(), [](const auto& v) { return static_cast<T>(v); });
	return *this;
}

template<typename T>
size_t Matrix<T>::rows() const noexcept
{
	return m_rows;
}

template<typename T>
size_t Matrix<T>::columns() const noexcept
{
	return m_columns;
}

template<typename T>
const T& Matrix<T>::at(size_t i, size_t j) const
{
	auto idx = i * m_columns + j;
	return m_values.at(idx);
}

template<typename T>
T& Matrix<T>::at(size_t i, size_t j)
{
	auto idx = i * m_columns + j;
	return m_values.at(idx);
}

template<typename T>
Matrix<T> Matrix<T>::transpose() const
{
	auto result = Matrix<T>(m_columns, m_rows);

	for (size_t row = 0; row < m_rows; ++row)
	{
		for (size_t col = 0; col < m_columns; ++col)
		{
			result.at(col, row) = at(row, col);
		}
	}

	return result;
}

template<typename T>
const std::vector<T>& Matrix<T>::data() const & noexcept
{
	return m_values;
}

template<typename T>
std::vector<T> Matrix<T>::data() && noexcept
{
	return std::move(m_values);
}

}