#pragma once

#include <cstdint>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>

#include <simulation/Matrix.hpp>

#ifndef SIMULATION_IMPL
#define SIMULATION_API __declspec(dllimport)
#else
#define SIMULATION_API __declspec(dllexport)
#endif // !SIMULATION_IMPL

namespace networks {
struct IEventLoop;

namespace simulation {

enum class ModelType
{
	Unknown,
	LinearActivation, //voting
	NonLinearActivation, //non-voting
};

struct SimulationState
{
	struct ClusterConnection
	{
		size_t from;
		size_t to;
		float lambda;
		float delta;
	};

	enum class Waveform
	{
		None,
		Square,
		Sine
	};

	Matrix<uint8_t> adjacencyMatrix;
	std::vector<size_t> clusterSizes;
	std::vector<ClusterConnection> clusterConnections;
	size_t hotCluster = 0;
	float mu = 0.0f; //extinguishing probability
	float gamma = 0.0f; //spontaneous activation probability
	unsigned thresholdNumber = 0;//number of active neighbors to activate the current node
	int period = 0;
	Waveform waveform = Waveform::None;
	ModelType modelType = ModelType::Unknown;
	unsigned long long steps = 0;

	static Waveform parseWaveform(const std::string& str)
	{
		if (str == "none")
			return Waveform::None;
		if (str == "square")
			return Waveform::Square;
		if (str == "sine")
			return Waveform::Sine;
		throw std::invalid_argument("invalid waveform name");
	}
};

struct ISimulation
{
	virtual ~ISimulation() = default;

	virtual std::pair<const float*, const float*> update() = 0;
};

namespace cuda {

inline ModelType parseModelType(const std::string& str)
{
	if (str == "linear")
		return ModelType::LinearActivation;
	if (str == "non-linear")
		return ModelType::NonLinearActivation;
	throw std::invalid_argument("invalid waveform name");
}

SIMULATION_API std::unique_ptr<ISimulation> CreateSimulation(SimulationState&& state, ModelType type, const std::shared_ptr<networks::IEventLoop>& eventLoop);

}
}
}