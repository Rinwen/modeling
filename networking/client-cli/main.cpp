//int main2(int argc, char** argv)
int main(int argc, char** argv)
{
	std::vector<float> stats;
	size_t delimiterPeriod = 0;
	try
	{
		auto config = networks::getConfig(argc, argv);
		auto simState = networks::createSimulationState(config);
		auto serializedState = networks::serializeState(simState);
		delimiterPeriod = simState.clusterSizes.size() + 1;

		using namespace boost::asio::ip;

		boost::asio::io_context io_ctx;
		tcp::resolver resolver(io_ctx);
		tcp::socket socket(io_ctx);

		boost::asio::connect(socket, resolver.resolve(
			std::any_cast<const std::string&>(config["host"]),
			std::any_cast<const std::string&>(config["port"])
		));
		
		size_t messageSize = serializedState.size();
		boost::asio::write(socket, boost::asio::buffer(&messageSize, sizeof(messageSize)), boost::asio::transfer_all());
		boost::asio::write(socket, boost::asio::buffer(serializedState), boost::asio::transfer_all());

		boost::asio::read(socket, boost::asio::buffer(&messageSize, sizeof(messageSize)), boost::asio::transfer_all());
		stats.resize(messageSize);
		boost::asio::read(socket, boost::asio::buffer(stats), boost::asio::transfer_all());
	}
	catch (const std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
	}

	for (size_t i = 0; i < stats.size(); ++i)
	{
		if (i != 0 && i % delimiterPeriod == 0)
		{
			std::cout << std::endl;
		}

		std::cout << stats[i] << ' ';
	}
	return 0;
}

//int main(int argc, char** argv)
//{
//	std::vector<std::thread> threads(16);
//
//	for (auto& t : threads)
//		t = std::thread(main2, argc, argv);
//
//	for (auto& t : threads)
//		t.join();
//	
//	return 0;
//}