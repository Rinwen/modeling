#pragma once

#include <SDKDDKVer.h>

#include <iostream>
#include <memory>

#include <libio/libio.hpp>
#include <simulation/simulation.hpp>

#define BOOST_DATE_TIME_NO_LIB
#define BOOST_REGEX_NO_LIB
#include <boost/asio.hpp>