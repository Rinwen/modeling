#include <libio/IEventLoop.hpp>
#include "Session.hpp"

namespace networks {

Session::Session(boost::asio::io_context& io_ctx, const std::shared_ptr<IEventLoop>& eventLoop) :
	m_socket(io_ctx),
	m_eventLoop(eventLoop)
{
}

void Session::run()
{
	try
	{
		size_t messageSize;
		boost::asio::read(m_socket, boost::asio::buffer(&messageSize, sizeof(messageSize)), boost::asio::transfer_all());

		std::vector<uint8_t> serializedState(messageSize);
		boost::asio::read(m_socket, boost::asio::buffer(serializedState), boost::asio::transfer_all());

		auto simState = deserializeState(serializedState);
		auto steps = simState.steps;
		auto modelType = simState.modelType;
		auto clusters = simState.clusterSizes.size();

		auto sim = networks::simulation::cuda::CreateSimulation(std::move(simState), modelType, m_eventLoop);

		std::vector<float> simResult;
		simResult.reserve(steps * (clusters + 1));

		for (unsigned long long i = 0; i < steps; ++i)
		{
			auto stepResult = sim->update();

			for (auto it = stepResult.first; it != stepResult.second; ++it)
				simResult.emplace_back(*it);
		}

		messageSize = simResult.size();
		boost::asio::write(m_socket, boost::asio::buffer(&messageSize, sizeof(messageSize)), boost::asio::transfer_all());
		boost::asio::write(m_socket, boost::asio::buffer(simResult), boost::asio::transfer_all());
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}
}

}