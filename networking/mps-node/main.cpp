#include "Session.hpp"

int main(int argc, char** argv)
{
	try
	{
		auto config = networks::parseArguments(argc, argv);
		auto port = std::stoi(config.at("port"));
		auto eventLoop = networks::createEventLoop();

		using namespace::boost::asio::ip;
		boost::asio::io_context io_ctx;
		tcp::acceptor acceptor(io_ctx, tcp::endpoint(tcp::v4(), port));

		while (true)
		{
			auto session = std::make_unique<networks::Session>(io_ctx, eventLoop);
			acceptor.accept(session->socket());

			std::thread(&networks::Session::run, std::move(session)).detach();
		}
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	return 0;
}