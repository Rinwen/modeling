#pragma once

#include <memory>

#include <boost/asio.hpp>

namespace networks {

struct IEventLoop;

class Session
{
public:
	Session(boost::asio::io_context& io_ctx, const std::shared_ptr<IEventLoop>& eventLoop);
	void run();

	boost::asio::ip::tcp::socket& socket() { return m_socket; };
private:
	boost::asio::ip::tcp::socket m_socket;
	std::shared_ptr<IEventLoop> m_eventLoop;
};

}