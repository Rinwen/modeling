@echo off

mkdir build-x64
cd build-x64

rem cmake ..
cmake -G "Visual Studio 16 2019" -A x64 ..

cmake --build . --config Release --parallel 16

cmake --install . --prefix ..

cd ..