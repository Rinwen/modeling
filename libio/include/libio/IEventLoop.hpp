#pragma once

#include <functional>
#include <memory>

#include "api.hpp"

namespace networks {

struct IEventLoop
{
	virtual ~IEventLoop() = default;
	virtual void enqueue(std::function<void()>&& task) = 0;
};

LIBIO_API std::shared_ptr<IEventLoop> createEventLoop();

}
