#pragma once

#include <any>
#include <map>
#include <string>

#include <libio/api.hpp>

namespace networks {

LIBIO_API std::map<std::string, std::string> parseArguments(int argc, char** argv);
LIBIO_API std::map<std::string, std::any> getConfig(int argc, char** argv);

}