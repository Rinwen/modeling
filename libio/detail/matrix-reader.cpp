#include "matrix-reader.hpp"

namespace networks {

static std::vector<float> readValues(size_t row, const std::string& str, size_t knownSize = 0)
{
	auto result = std::vector<float>();
	result.reserve(knownSize);

	auto stream = std::stringstream(str, std::ios_base::in);

	for(size_t column = 0;; column++)
	{
		auto value = 0.0f;
		stream >> value;

		if (stream.eof() && stream.fail())
			break;
		if (stream.fail())
			throw std::invalid_argument("unexpected value at position (" + std::to_string(row) + ','+ std::to_string(column) + ')');

		result.emplace_back(value);
	}

	return result;
}

static std::vector<std::vector<float>> readRows(std::ifstream& stream)
{
	auto result = std::vector<std::vector<float>>();

	for(size_t row = 0;;row++)
	{
		auto line = std::string();
		std::getline(stream, line);
		if (stream.eof() && stream.fail())
			break;
		if (stream.fail())
			throw std::invalid_argument("unexpected failure while reading row #" + std::to_string(row));

		auto knownSize = size_t(0);
		if (!result.empty())
		{
			knownSize = result.front().size();
		}
		result.emplace_back(readValues(row, line, knownSize));
	}

	return result;
}

Matrix<float> readMatrix(const std::filesystem::path& path)
{
	auto file = std::ifstream(path);

	if (file.fail())
		throw std::invalid_argument("cannot open " + path.string());

	try
	{
		auto rows = readRows(file);
		if (rows.empty())
			throw std::invalid_argument("empty file");

		auto precedingIllRow = std::adjacent_find(TRY_PARALLELIZE rows.cbegin(), rows.cend(), [](const auto& lhs, const auto& rhs)
		{
			return lhs.size() != rhs.size();
		});

		if (precedingIllRow != rows.cend())
			throw std::invalid_argument("row #" + std::to_string(std::distance(rows.cbegin(), precedingIllRow + 1)) + " is not a valid size long");

		auto result = Matrix<float>(rows.size(), rows.front().size());

		for (size_t i = 0; i < rows.size(); ++i)
		{
			const auto& row = rows[i];
			for (size_t j = 0; j < row.size(); ++j)
			{
				result.at(i, j) = row.at(j);
			}
		}

		return result;
	}
	catch (const std::invalid_argument& ex)
	{
		throw std::invalid_argument(path.string() + " parsing failed due to reason: " + ex.what());
	}
}

}