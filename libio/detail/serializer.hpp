#pragma once

#include <libio/api.hpp>

#include <simulation/simulation.hpp>

namespace networks {

LIBIO_API std::vector<uint8_t> serializeState(const simulation::SimulationState& state);
LIBIO_API simulation::SimulationState deserializeState(std::vector<uint8_t>& state);

}