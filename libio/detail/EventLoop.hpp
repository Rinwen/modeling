#pragma once

#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>
#include <vector>

#include <libio/IEventLoop.hpp>

namespace networks {

class EventLoop : public IEventLoop
{
public:
	EventLoop();
	EventLoop(const EventLoop&) = delete;
	EventLoop(EventLoop&&) = delete;
	virtual ~EventLoop() override;

	virtual void enqueue(std::function<void()>&& task) override;
private:
	using task_type_t = std::pair<std::thread::id, std::function<void()>>;
	std::vector<task_type_t> m_writeBuffer;
	std::vector<task_type_t> m_readBuffer;
	std::mutex m_mutex;
	std::condition_variable m_condVar;
	std::thread m_thread;
	bool m_running = true;

	void threadFunc();
	static void executeTask(const task_type_t& task) noexcept;
};

}
