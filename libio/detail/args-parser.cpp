#include "args-parser.hpp"

//arguments example:

//networks.exe
//	--matrix=matrices/adj_p1=0.45p2=1n1=168n2=8.txt
//	--clusters=(168,8,168)
//	--hot-cluster=1
//	--steps=160000
//	--mu=0.05
//	--lambda=matrices/lambda.txt
//	--type=non-linear
//	[--threshold=1]
//	[--period=0 [--waveform=square]]
//	[--gamma=0]
//
// for client-cli:
// --host=vasin.space
// --port=2504 | --port=3105

namespace networks {

static std::filesystem::path make_path(const std::string& str)
{
	auto path = std::filesystem::path(str);
	if (!std::filesystem::exists(path) || std::filesystem::is_directory(path))
		throw std::invalid_argument("no such file");
	return path;
}

static std::vector<int> stoivec(const std::string& vector)
{
	static const auto listRegex = std::regex("\\((.+)\\)");
	static const auto headTailRegex = std::regex("\\s*(\\d+)\\s*(,(.+))?");

	auto result = std::vector<int>();
	auto listMatch = std::smatch();
	if (!std::regex_match(vector, listMatch, listRegex))
		throw std::invalid_argument("invalid vector format");

	auto list = std::string(listMatch[1]);

	while (true)
	{
		auto headTailMatch = std::smatch();
		if (!std::regex_match(list, headTailMatch, headTailRegex))
			throw std::invalid_argument("invalid vector format");

		if (headTailMatch[3].length())
		{
			result.emplace_back(std::stoi(headTailMatch[1]));
			list = headTailMatch[3];
		}
		else if (headTailMatch[1].length())
		{
			result.emplace_back(std::stoi(headTailMatch[1]));
			break;
		}
		else
		{
			throw std::invalid_argument("invalid vector format");
		}
	}

	return result;
}

static int stoi(const std::string& str) { return std::stoi(str); }

static unsigned long long stoull(const std::string& str) { return std::stoull(str); }

static float stof(const std::string& str) { return std::stof(str); }

static std::string scopy(const std::string& str) { return str; }

template<typename TFunc>
static void parse(const std::string& name, const std::map<std::string, std::string>& arguments, std::map<std::string, std::any>& config, TFunc&& f)
{
	try
	{
		auto it = arguments.find(name);
		if (it == arguments.cend())
			throw std::invalid_argument("missing expected argument");
		if (config.count(name))
			throw std::invalid_argument("argument redefinition");

		config.emplace(name, f(it->second));
	}
	catch (const std::invalid_argument& ex)
	{
		throw std::invalid_argument("\"" + name + "\"" + " argument is invalid: " + ex.what());
	}
}

template<typename TFunc>
static bool tryParse(const std::string& name, const std::map<std::string, std::string>& arguments, std::map<std::string, std::any>& config, TFunc&& f, const std::string& value)
{
	try
	{
		if (config.count(name))
			throw std::invalid_argument("argument redefinition");

		auto it = arguments.find(name);
		if (it != arguments.cend())
		{
			config.emplace(name, f(it->second));
			return true;
		}
		else
		{
			config.emplace(name, f(value));
			return false;
		}
	}
	catch (const std::invalid_argument& ex)
	{
		throw std::invalid_argument("\"" + name + "\"" + " argument is invalid: " + ex.what());
	}
}

std::map<std::string, std::string> parseArguments(int argc, char** argv)
{
	static const auto regex = std::regex("--(\\w[\\w-]*)=(.+)");
	auto result = std::map<std::string, std::string>();

	std::for_each(argv + 1, argv + argc, [&](const char* arg)
	{
		auto match = std::cmatch();
		if (!std::regex_match(arg, match, regex))
			return;

		result[match[1]] = match[2];
	});

	return result;
}

std::map<std::string, std::any> getConfig(int argc, char** argv)
{
	auto stringArguments = parseArguments(argc, argv);

	auto result = std::map<std::string, std::any>();

	parse("matrix", stringArguments, result, &networks::make_path);
	parse("clusters", stringArguments, result, &networks::stoivec);
	parse("hot-cluster", stringArguments, result, &networks::stoi);
	parse("steps", stringArguments, result, &networks::stoull);
	parse("mu", stringArguments, result, &networks::stof);
	parse("lambda", stringArguments, result, &networks::make_path);
	parse("type", stringArguments, result, &networks::scopy);
	
	tryParse("gamma", stringArguments, result, &networks::stof, "0");
	tryParse("threshold", stringArguments, result, &networks::stoi, "1");
	tryParse("port", stringArguments, result, &networks::scopy, "");
	tryParse("host", stringArguments, result, &networks::scopy, "");

	if (tryParse("period", stringArguments, result, &networks::stoi, "0"))
	{
		tryParse("waveform", stringArguments, result, &networks::scopy, "square");
	}
	else if (stringArguments.count("waveform"))
	{
		throw std::invalid_argument("\"waveform\" argument can be passed only if \"period\" is defined");
	}
	else
	{
		result["waveform"] = std::string("none");
	}

	return result;
}

}