#include <libio/libio.hpp>

static void validateGraph(const networks::Matrix<uint8_t>& adjacencyMat, const std::vector<int>& clusters)
{
	if (adjacencyMat.rows() != adjacencyMat.columns() || adjacencyMat.rows() < 1 || adjacencyMat.columns() < 1)
		throw std::invalid_argument("invalid adjacency matrix size");

	if (std::any_of(clusters.cbegin(), clusters.cend(), [](auto clusterSize) { return clusterSize < 1; }))
		throw std::invalid_argument("invalid cluster size");

	auto nodes = adjacencyMat.rows();
	if (nodes != std::accumulate(clusters.cbegin(), clusters.cend(), 0))
		throw std::invalid_argument("inconsistent matrix size and clusters size");
}

static std::vector<networks::simulation::SimulationState::ClusterConnection> parseConnections(const std::vector<int>& clusters, const networks::Matrix<float>& lambdaMat)
{
	using namespace networks::simulation;
	if (lambdaMat.columns() != 4)
		throw std::invalid_argument("invalid lambda matrix width");

	auto result = std::vector<SimulationState::ClusterConnection>(lambdaMat.rows());

	for (size_t row = 0; row < lambdaMat.rows(); ++row)
	{
		auto from = int(lambdaMat.at(row, 0));
		auto to = int(lambdaMat.at(row, 1));
		auto lambda = lambdaMat.at(row, 2);
		auto delta = lambdaMat.at(row, 3);

		if (from < 0 || from >= clusters.size())
			throw std::invalid_argument("invalid outcome cluster in lambda matrix at row #" + std::to_string(row));

		if (to < 0 || to >= clusters.size())
			throw std::invalid_argument("invalid income cluster in lambda matrix at row #" + std::to_string(row));

		if (lambda < 0.0f || lambda > 1.0f)
			throw std::invalid_argument("invalid propablity in lambda matrix at row #" + std::to_string(row));

		if ((lambda + delta) < 0.0f || (lambda + delta) > 1.0f)
			throw std::invalid_argument("invalid delta in lambda matrix at row #" + std::to_string(row));

		result[row] = { size_t(from), size_t(to), lambda, delta };
	}

	return result;
}

networks::simulation::SimulationState networks::createSimulationState(std::map<std::string, std::any> parameters)
{
	using namespace networks::simulation;

	auto adjacencyMat = networks::Matrix<uint8_t>(networks::readMatrix(std::any_cast<const std::filesystem::path&>(parameters["matrix"])));
	auto lambdaMat = networks::readMatrix(std::any_cast<const std::filesystem::path&>(parameters["lambda"]));
	auto clusters = std::any_cast<std::vector<int>&&>(std::move(parameters["clusters"]));
	auto hotCluster = std::any_cast<int>(parameters["hot-cluster"]);
	auto mu = std::any_cast<float>(parameters["mu"]);
	auto gamma = std::any_cast<float>(parameters["gamma"]);
	auto period = std::any_cast<int>(parameters["period"]);
	auto waveform = SimulationState::parseWaveform(std::any_cast<const std::string&>(parameters["waveform"]));
	auto modelType = cuda::parseModelType(std::any_cast<const std::string&>(parameters["type"]));
	auto steps = std::any_cast<unsigned long long>(parameters["steps"]);
	auto threshold = std::any_cast<int>(parameters["threshold"]);

	validateGraph(adjacencyMat, clusters);
	auto connections = parseConnections(clusters, lambdaMat);
	if (mu < 0.0f || mu > 1.0f)
		throw std::invalid_argument("invalid mu value");
	if (hotCluster < 0 || hotCluster >= clusters.size())
		throw std::invalid_argument("invalid hot-cluster value");

	if (period == 0)
		waveform = SimulationState::Waveform::None;

	return
	{
		std::move(adjacencyMat),
		std::vector<size_t>(clusters.cbegin(), clusters.cend()),
		std::move(connections),
		size_t(hotCluster),
		mu,
		gamma,
		unsigned(threshold),
		period,
		waveform,
		modelType,
		steps
	};
}
