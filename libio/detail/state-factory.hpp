#pragma once

#include <any>
#include <map>
#include <string>

#include <simulation/simulation.hpp>

#include "api.hpp"

namespace networks{

LIBIO_API simulation::SimulationState createSimulationState(std::map<std::string, std::any> parameters);

}