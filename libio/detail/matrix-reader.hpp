#pragma once

#include <any>
#include <filesystem>

#include <simulation/Matrix.hpp>

#include <libio/api.hpp>

namespace networks {

LIBIO_API Matrix<float> readMatrix(const std::filesystem::path& path);

}