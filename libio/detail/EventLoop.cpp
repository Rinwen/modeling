#include "EventLoop.hpp"

namespace networks {

std::shared_ptr<IEventLoop> createEventLoop()
{
	return std::make_shared<EventLoop>();
}

EventLoop::EventLoop()
{
	m_thread = std::thread(&EventLoop::threadFunc, this);
}

EventLoop::~EventLoop()
{
	enqueue([&]
	{
		m_running = false;
	});

	m_thread.join();
}

void EventLoop::enqueue(std::function<void()>&& task)
{
	{
		auto id = std::this_thread::get_id();
		std::lock_guard<std::mutex> lock(m_mutex);
		m_writeBuffer.emplace_back(task_type_t(id, std::move(task)));
	}
	m_condVar.notify_one();
}

void EventLoop::threadFunc()
{
	while (m_running)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		m_condVar.wait(lock, [&]()
		{
			return !m_writeBuffer.empty();
		});

		std::swap(m_writeBuffer, m_readBuffer);
		lock.unlock();

		std::map<std::thread::id, std::vector<task_type_t>::const_iterator> roundRobin;
		for (auto it = m_readBuffer.cbegin(); it != m_readBuffer.cend(); ++it)
			roundRobin.emplace(it->first, it);

		while (!roundRobin.empty())
		{
			for (auto pairIt = roundRobin.begin(); pairIt != roundRobin.end();)
			{
				executeTask(*pairIt->second);
				auto searchIt = std::find_if(pairIt->second + 1, m_readBuffer.cend(), [&](const task_type_t& task)
				{
					return task.first == pairIt->second->first;
				});

				if (searchIt != m_readBuffer.cend())
				{
					pairIt->second = searchIt;
					++pairIt;
				}
				else
				{
					auto toDelete = pairIt;
					pairIt++;
					roundRobin.erase(toDelete);
				}
			}
		}

		/*for (const auto& task : m_readBuffer)
		{
			executeTask(task);
		}*/

		m_readBuffer.clear();
	}
}

void EventLoop::executeTask(const task_type_t& task) noexcept
{
	try
	{
		task.second();
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}
}

}