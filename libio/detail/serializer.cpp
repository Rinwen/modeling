#include "serializer.hpp"

namespace networks {

template<typename T>
static void writeValue(std::vector<uint8_t>& buffer, const T& value)
{
	const auto begin = reinterpret_cast<const uint8_t*>(&value);
	const auto end = reinterpret_cast<const uint8_t*>((&value) + 1);

	std::copy(begin, end, std::back_inserter(buffer));
}

template<typename T>
static void writeArray(std::vector<uint8_t>& buffer, const std::vector<T>& arr)
{
	writeValue(buffer, arr.size());

	const auto begin = reinterpret_cast<const uint8_t*>(arr.data());
	const auto end = reinterpret_cast<const uint8_t*>(arr.data() + arr.size());
	std::copy(begin, end, std::back_inserter(buffer));
}

template<typename T>
static void writeMatrix(std::vector<uint8_t>& buffer, const Matrix<T>& mat)
{
	writeValue(buffer, mat.rows());
	writeValue(buffer, mat.columns());
	writeArray(buffer, mat.data());
}

template<typename T>
static T readValue(std::vector<uint8_t>& buffer)
{
	if (buffer.size() < sizeof(T))
		throw std::invalid_argument("the buffer size is less than expected");
	T result = *reinterpret_cast<const T*>(buffer.data());
	buffer.erase(buffer.begin(), buffer.begin() + sizeof(T));
	return result;
}

template<typename T>
static std::vector<T> readArray(std::vector<uint8_t>& buffer)
{
	const auto elements = readValue<size_t>(buffer);
	const auto kBytesSize = sizeof(T) * elements;

	if (buffer.size() < kBytesSize)
		throw std::invalid_argument("the buffer size is less than expected");

	const auto cbegin = reinterpret_cast<const T*>(buffer.data());
	const auto cend = cbegin + elements;

	std::vector<T> result(elements);
	std::copy(cbegin, cend, result.begin());
	buffer.erase(buffer.begin(), buffer.begin() + kBytesSize);

	return result;
}

template<typename T>
static Matrix<T> readMatrix(std::vector<uint8_t>& buffer)
{
	auto rows = readValue<size_t>(buffer);
	auto columns = readValue<size_t>(buffer);
	auto values = readArray<T>(buffer);

	return Matrix<T>
	{
		rows,
		columns,
		std::move(values)
	};
}

std::vector<uint8_t> serializeState(const simulation::SimulationState& state)
{
	std::vector<uint8_t> result;

	//adjacencyMatrix
	writeMatrix(result, state.adjacencyMatrix);

	//clusterSizes
	writeArray(result, state.clusterSizes);

	//clusterConnections
	writeArray(result, state.clusterConnections);

	//scalars
	writeValue(result, state.hotCluster);
	writeValue(result, state.mu);
	writeValue(result, state.gamma);
	writeValue(result, state.thresholdNumber);
	writeValue(result, state.period);
	writeValue(result, state.waveform);
	writeValue(result, state.modelType);
	writeValue(result, state.steps);

	return result;
}
simulation::SimulationState deserializeState(std::vector<uint8_t>& state)
{
	using namespace simulation;
	return SimulationState
	{
		readMatrix<uint8_t>(state),
		readArray<size_t>(state),
		readArray<SimulationState::ClusterConnection>(state),
		readValue<size_t>(state),
		readValue<float>(state),
		readValue<float>(state),
		readValue<unsigned>(state),
		readValue<int>(state),
		readValue<SimulationState::Waveform>(state),
		readValue<ModelType>(state),
		readValue<unsigned long long>(state)
	};
}

}